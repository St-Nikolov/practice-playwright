import { type Locator, type Page } from '@playwright/test';

export class HomePage {
    private readonly page: Page;
    readonly url: string;

    readonly leadImageLocator: Locator;

    constructor (page: Page) {
        this.page = page;
        this.url = "https://practicesoftwaretesting.com/#/";
        this.leadImageLocator = page.getByRole('img', { name: 'Banner' });
    }

    async goto() {
        await this.page.goto(this.url);
    }
}