import { type Locator, type Page } from '@playwright/test';

export class ProductCard{
    private readonly page: Page;
    private cardDataTestPrefix: string;

    constructor(page: Page) {
        this.page = page;
        this.cardDataTestPrefix = "product-"
    }

    async getAllCards(): Promise<Locator> {
        const allCardsSelector = `.card[data-test^='${this.cardDataTestPrefix}']`
        await this.page.waitForSelector(allCardsSelector);
        return this.page.locator(allCardsSelector); 
    }
}