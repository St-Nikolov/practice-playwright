import { type Locator, type Page } from '@playwright/test';

export class BaseUIObject {
    protected readonly page:Page;

    constructor (page: Page){
        this.page = page;
    }

    getUrl() {
        return this.page.url()
    }
}
