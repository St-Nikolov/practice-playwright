import { type Locator, type Page } from '@playwright/test';
import { BaseUIObject } from './base.ui-object';

export class TopBar extends BaseUIObject{
    private readonly topBarContainerLocator: Locator;
    constructor (page:Page) {
        super(page);
        this.topBarContainerLocator = page.locator('app-header')

    }

    getMenuItem(label:string) :Locator {
        return this.getAllMenuItems().getByText(label);
    }

    getAllMenuItems() :Locator {
        return this.topBarContainerLocator.getByRole('listitem');
    }

}
