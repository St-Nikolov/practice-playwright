import { type Locator, type Page } from '@playwright/test';
import { BaseUIObject } from './base.ui-object';

export class SignInPage extends BaseUIObject{
    readonly pageUrl = "https://practicesoftwaretesting.com/#/auth/login"; 
    readonly pageHeading:Locator;
    
    constructor (page:Page) {
        super(page);
        this.pageHeading = page.getByRole('heading', {name: "Login"});
    }
}
