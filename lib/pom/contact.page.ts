import { type Locator, type Page } from '@playwright/test';
import { BaseUIObject } from './base.ui-object';

export class ContactPage extends BaseUIObject{
    readonly pageUrl = "https://practicesoftwaretesting.com/#/contact"; 
    readonly pageHeading:Locator;
    
    constructor (page:Page) {
        super(page);
        this.pageHeading = page.getByRole('heading', {name: "Contact"})
    }
}
