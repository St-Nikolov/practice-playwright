import { test, expect } from '@playwright/test';
import { HomePage } from '../lib/pom/home.page';
import { ProductCard } from '../lib/pom/product.card';
import { TopBar } from '../lib/pom/top-bar';
import { ContactPage } from '../lib/pom/contact.page';
import { SignInPage } from '../lib/pom/sign-in.page';

test.describe ('Home page tests', () => {

    let homePage: HomePage;
    test.beforeEach(async ({page}) => {
        homePage = new HomePage(page);
        await homePage.goto();
    });

    test('Home page lead image is visible', async ({page}) => {
        await expect(homePage.leadImageLocator,'Home page lead image should be visible').toBeVisible();
    });
    
    test('Proper number of product cards are shown on the home page', async ({page}) => {
        const expectedCardsNumber = 9;
        const productCard = new ProductCard(page);
    
        expect((await productCard.getAllCards()),`${expectedCardsNumber} product cards should be shown on the home page`).toHaveCount(expectedCardsNumber);
    });
    
    test('Propper menu items are presented in the page top bar', async ({page}) => {
        const expectedMenuLabels = ['Home','Categories','Contact','Sign in'];
        const topBar = new TopBar(page);
    
        await expect.soft(topBar.getAllMenuItems(),`${expectedMenuLabels.length} menu items are presented in the page top bar`).toHaveCount(expectedMenuLabels.length);
        for (let i=0; i<expectedMenuLabels.length; i++) {
            await expect.soft(topBar.getMenuItem(expectedMenuLabels[i]),`${expectedMenuLabels[i]} menu item is visible in the page top bar`).toBeVisible();
        }
    });

    test('Clicking on "Contact" in the page top bar navigates to Contact page',async ({page}) => {
        const contactPage = new ContactPage(page);
        const topBar = new TopBar(page);
        
        await topBar.getMenuItem("Contact").click();

        await expect.soft(contactPage.pageHeading,'Contact page heading is visible').toBeVisible();
        expect.soft(page.url(),`Page url should be ${contactPage.pageUrl}`).toBe(contactPage.pageUrl);
    })

    test('Clicking on "Sign in" in the page top bar navigates to Sign in page',async ({page}) => {
        const signInPage = new SignInPage(page);
        const topBar = new TopBar(page);
        
        await topBar.getMenuItem("Sign in").click();

        await expect.soft(signInPage.pageHeading,'Sign in page heading is visible').toBeVisible();
        expect.soft(page.url(),`Page url should be ${signInPage.pageUrl}`).toBe(signInPage.pageUrl);
    })

});
